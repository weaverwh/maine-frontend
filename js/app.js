var app = angular.module('CitizenScience', ['ui.router', 'ui.bootstrap', 'ui-leaflet', 'Routing', 'rzModule', 'nemLogging']);

app.run(function ($rootScope, $location, $window, $timeout) {

  angular.element(document).ready(function(){

    function checkScrollPosition(){

      function isScrolledIntoView(elem){
  			if ($(elem).offset()) {
          var docViewTop = $(window).scrollTop();
          var docViewBottom = docViewTop + $(window).height();
          var elemTop = $(elem).offset().top;
          var elemBottom = elemTop + $(elem).height();
          return ((elemTop <= docViewBottom));
  			} else {
  				return false;
  			}
      }

      if (isScrolledIntoView($('.aboutContainer img'))) {
        $('.aboutContainer p:first-of-type').addClass('active');
        $timeout(function(){
          $('.aboutContainer p:last-of-type').addClass('active');
          $timeout(function(){
            $('.aboutContainer img').addClass('active');
          }, 300);
        }, 300);
      }

      if (isScrolledIntoView($('.footerContainer'))) {
        var footerContainers = $('.footerContainer .col-md-3');
        $(footerContainers[0]).addClass('active');
        $timeout(function(){
          $(footerContainers[1]).addClass('active');
          $timeout(function(){
            $(footerContainers[2]).addClass('active');
            $timeout(function(){
              $(footerContainers[3]).addClass('active');
            }, 300);
          }, 300);
        }, 300);
      }

    }

    $(document).on('scroll', function(e){
      checkScrollPosition();
    });

    $('.indexLoader').addClass('ready');

    $timeout(function(){
			$('.indexLoader').addClass('active');
    }, 100);

  });

	// $window.ga('create', 'UA-70978264-1', 'auto');
  // $rootScope.$on('$stateChangeSuccess', function(event){
  //     $window.ga('send', 'pageview', $location.path());
  // });

});
