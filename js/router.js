(function() {

	var RoutingModule = angular.module('Routing', []);

	RoutingModule.config(function($stateProvider, $urlRouterProvider, $locationProvider) {

		$urlRouterProvider.otherwise("/");
		$locationProvider.html5Mode(false).hashPrefix('');

		$stateProvider
			.state('home', {
				url: "/",
				templateUrl: "templates/home.html",
				controller: "HomeController"
			})
			.state('programs', {
				url: "/programs",
				templateUrl: "templates/programs.html"
			})
			.state('about', {
				url: "/about",
				templateUrl: "templates/about.html"
			})
			.state('news', {
				url: "/news",
				templateUrl: "templates/news.html"
			})
			.state('contact', {
				url: "/contact",
				templateUrl: "templates/contact.html"
			})
			.state('heronObservationNetwork', {
				url: "/programs/heronObservationNetwork",
				templateUrl: "templates/programs/heronObservationNetwork.html",
				controller: "HeronObservationNetworkController"
			})
			.state('riverBirdAtlas', {
				url: "/programs/riverBirdAtlas",
				templateUrl: "templates/programs/riverBirdAtlas.html",
				controller: "RiverBirdAtlasController"
			})
	});

})();
