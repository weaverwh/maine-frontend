app.controller('HomeController', ['$scope', '$state', '$timeout',
	function($scope, $state, $timeout) {

	$(window).scrollTop(0);

  $('.programPreviewContent').hover(
    function(e){
      var thisParagraph = $(e.target).find('p');
      $(thisParagraph).textillate({
        autoStart: false,
        in: {
          effect: 'flipInX',
          delay: 5
        }
      });
      $(thisParagraph).textillate('start');
    },
    function(e){
      console.log(e);
    }
  );

  angular.element(document).ready(function(){

    $timeout(function(){

	    $timeout(function(){
	      $('.heroText h2').textillate({
	        in: {
	          effect: 'flipInX',
	          delay: 80
	        }
	      });
	      $('.heroText h2').addClass('active');
	    }, 500);

	    $timeout(function(){
	      $(".rslides").responsiveSlides({
	        auto: true,             // Boolean: Animate automatically, true or false
	        speed: 1000,            // Integer: Speed of the transition, in milliseconds
	        timeout: 6000,          // Integer: Time between slide transitions, in milliseconds
	        pager: true,           // Boolean: Show pager, true or false
	        nav: false,             // Boolean: Show navigation, true or false
	        random: false,          // Boolean: Randomize the order of the slides, true or false
	        pause: false,           // Boolean: Pause on hover, true or false
	        pauseControls: true,    // Boolean: Pause when hovering controls, true or false
	        prevText: "Previous",   // String: Text for the "previous" button
	        nextText: "Next",       // String: Text for the "next" button
	        maxwidth: "",           // Integer: Max-width of the slideshow, in pixels
	        navContainer: "",       // Selector: Where controls should be appended to, default is after the 'ul'
	        manualControls: "",     // Selector: Declare custom pager navigation
	        namespace: "rslides",   // String: Change the default namespace used
	        before: function(){},   // Function: Before callback
	        after: function(){}     // Function: After callback
	      });

	      $('.homeLoader').removeClass('active');
				$('.heroText h2').addClass('in');
	      $('.heroText h1').textillate({
	        in: {
	          effect: 'flipInY',
	          delay: 25,
	          shuffle: true
	        }
	      });
	    }, 2700);

		}, 100);

  });

}]);
