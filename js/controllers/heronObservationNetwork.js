app.controller('HeronObservationNetworkController', ['$scope', '$state', '$timeout', 'leafletData', '$uibModal',
	function($scope, $state, $timeout, leafletData, $uibModal) {

		$(window).scrollTop(0);

		$scope.inquire = function(){
			$('#inquireModal').modal('show');
		}

		angular.extend($scope, {
			center: {
				lat: 43.7965,
				lng: -70.2589,
				zoom: 8
			},
			defaults: {
				scrollWheelZoom: false,
				zoomControl: false
			}
		});

		leafletData.getMap('map').then(function(map){
			console.log(map);
			// L.GeoIP.centerMapOnPosition(map, 15);
			L.control.zoom({
			  position:'bottomleft'
			}).addTo(map);
		});

		$scope.slider = {
		  min: 2000,
		  max: 2017,
		  options: {
		    floor: 2000,
		    ceil: 2017
		  }
		};

		if (!String.prototype.trim) {
			(function() {
				// Make sure we trim BOM and NBSP
				var rtrim = /^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g;
				String.prototype.trim = function() {
					return this.replace(rtrim, '');
				};
			})();
		}

		[].slice.call( document.querySelectorAll( '.input__field' ) ).forEach( function( inputEl ) {
			// in case the input is already filled..
			if( inputEl.value.trim() !== '' ) {
				$(inputEl.parentNode).addClass('input--filled');
			}

			// events:
			inputEl.addEventListener( 'focus', onInputFocus );
			inputEl.addEventListener( 'blur', onInputBlur );
		} );

		function onInputFocus( ev ) {
			$(ev.target.parentNode).addClass('input--filled');
		}

		function onInputBlur( ev ) {
			if( ev.target.value.trim() === '' ) {
				$(ev.target.parentNode).removeClass('input--filled');
			}
		}

}]);
